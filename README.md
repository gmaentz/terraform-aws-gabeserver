# Cloud Server Module

This repo contains module to add a web server for perform load balancing on [AWS](https://aws.amazon.com) using [Amazon EC2 Server](https://aws.amazon.com/ec2/).

## Server Architecture

![Server EC2 Architecture](https://timesofcloud.com/wp-content/uploads/2017/09/Amazon_EC2_Instance.png "Server EC2 Architecture")

## Features

- Build a Web EC2 Server

### Core concepts

- [What is EC2](https://github.com/gruntwork-io/terraform-google-load-balancer/blob/master/modules/http-load-balancer/core-concepts.md#what-is-cloud-load-balancing)

### Repo organisation

This repo has the following folder structure:

* [root](https://github.com/gruntwork-io/terraform-google-load-balancer/tree/master): The root folder contains an example of how to deploy an EC2 Server with multiple backends. See [http-multi-backend example documentation](https://github.com/gruntwork-io/terraform-google-load-balancer/blob/master/examples/http-multi-backend) for the documentation.

* [modules](https://github.com/gruntwork-io/terraform-google-load-balancer/blob/master/modules): This folder contains the main implementation code for this Module.

  The primary modules are:

    * [Ubuntu Web Server](https://tfe.couchtocloud.com/app/FAA/modules/view/gabeserver/aws) is used to create an [Ubuntu EC2 Server](https://aws.amazon.com/ec2/).
                                                                                                                                           

## Deploy

If you want to try this repo out for experimenting and learning, check out the following resources:

- [examples folder](https://github.com/gruntwork-io/terraform-google-load-balancer/blob/master/examples): The `examples` folder contains sample code optimized for learning, experimenting, and testing.

## License

Please see [LICENSE](https://github.com/gruntwork-io/terraform-google-load-balancer/blob/master/LICENSE.txt) for details on how the code in this repo is licensed.